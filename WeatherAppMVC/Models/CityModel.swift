//
//  CityModel.swift
//  WeatherAppMVC
//
//  Created by OSX on 9/10/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import Foundation
import RealmSwift


class CityModel: Object, Codable {
  @objc dynamic var id = 0
  @objc dynamic var name = ""
  @objc dynamic var  country_code = ""
   private enum CodingKeys: String, CodingKey {
        case name, country_code = "country", id
    }
    
    
}
