//
//  ParseCitiesJsonFile.swift
//  WeatherAppMVC
//
//  Created by OSX on 9/9/19.
//  Copyright © 2019 eslam. All rights reserved.
//
import Foundation
import RealmSwift


class WeatherDetails : Object , Codable{
   @objc dynamic var city : CityModel?
    dynamic var weatherInfo  = List<WeatherInfo>()
    private enum CodingKeys: String, CodingKey {
        case  weatherInfo = "list", city = "city"
    }
    
    }

class WeatherInfo : Object , Codable{
    dynamic var weather  = List<Weather>()
   @objc dynamic var aboutTemperature :  AboutTemperature?
   @objc dynamic var dateText: String = ""
    private enum CodingKeys: String, CodingKey {
        case  weather, aboutTemperature = "main" , dateText = "dt_txt"
    }
//    required convenience init(from decoder: Decoder) throws {
//    self.init()
//    let container = try decoder.container(keyedBy: CodingKeys.self)
//    let weathers = try container.decode([Weather].self, forKey: .weather)
//     cityId = try container.decode(Int.self, forKey: .cityId)
//     cityName = try container.decode(String.self, forKey: .cityName)
//     aboutTemperature = try container.decode(AboutTemperature.self, forKey: .aboutTemperature)
//     contryCode = try container.decode(ContryCode.self, forKey: .contryCode)
//
//    weather.append(objectsIn: weathers)
//}
//

}

class Weather: Object, Codable {
    @objc  dynamic var _description: String = ""
    @objc dynamic var icon: String = ""
    private enum CodingKeys: String, CodingKey {
        case icon, _description = "description"
    }
  
}

class AboutTemperature: Object, Codable {
    @objc  dynamic var temp: Double = 0.0
    @objc   dynamic var tempMax: Double = 0.0
    @objc   dynamic var tempMin: Double = 0.0
    private enum CodingKeys: String, CodingKey {
        case temp, tempMax = "temp_max", tempMin = "temp_min"
    }
}

class ContryCode:  Object, Codable {
 @objc  dynamic  var country: String = ""
}

extension WeatherInfo  {
    var iconUrlPath: String {
        guard let iconName = weather.first?.icon else { return "" }
        return "https://openweathermap.org/img/w/\(iconName).png"
    }
    
    var displayTemperature: Int {
        let value = Int(round(aboutTemperature!.temp))
        return value
    }
    
    var displayTemperatureMax: Int {
        let value = Int(round(aboutTemperature!.tempMax))
        return value
    }
    
    var displayTemperatureMin: Int {
        let value = Int(round(aboutTemperature!.tempMin))
        return value
    }
    
    var weatherDescription: String {
        guard let weatherDescription = weather.first?._description else { return "" }
        return weatherDescription
    }
}



