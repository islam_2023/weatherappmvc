//
//  ViewController.swift
//  WeatherAppMVC
//
//  Created by OSX on 9/9/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import UIKit
import CoreLocation
import SVProgressHUD
import PopupDialog

class HomeViewController: UIViewController {
    
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var cities = [CityModel]()
    var locationManager = CLLocationManager()
    var hideAddButtonDelegate : HideAddButton?
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAllCities()
        setupTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg")!)
        
    }
    
    func setupTable() {
        
        if  AppDelegate.isApplicationFirstLaunch {
            setupLocation()
        }
        else {
            loadUpdatedCities()
        }
        
    }
    
    func loadAllCities () {
        JsonHelper.loadAllCitiesFromFile { [ weak self]( check , result) in
            if let _ = check {
                
            }
            else {
                DispatchQueue.main.async {
                    self?.dropDownSetUP(cities: result!)
                }
            }
        }
        
    }
    
    
    
    func dropDownSetUP( cities : [CityModel])  {
        
        let dropDownTop = VPAutoComplete()
        let citiesName = cities.map { $0.name }
        dropDownTop.dataSource = citiesName
        dropDownTop.onTextField = cityTextField
        dropDownTop.onView = self.view
        dropDownTop.didEndText(textField: cityTextField)
        //        dropDownTop.showAlwaysOnTop = true //To show dropdown always on top.
        dropDownTop.show { (str, index) in
            print("string : \(str) and Index : \(index)")
            self.cityTextField.text = str
            self.getForecastWeather(cityId : cities[index].id)
        }
    }
    
    func loadUpdatedCities() {
        let weathers = RealmHelper.shared.loadAllWeather()
        self.cities = weathers.map{
            $0.city!
        }
        tableView.reloadData()
    }
    
    
}

extension HomeViewController : CLLocationManagerDelegate {
    
    func  setupLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        let  lat = String(userLocation.coordinate.latitude)
        let  lng = String(userLocation.coordinate.longitude)
        
        SVProgressHUD.show()
        ServiceManager.shared.getForecastWeather(by: lat, lng: lng) { (weather) in
        SVProgressHUD.dismiss()

            if let _ =  weather{
            RealmHelper.shared.addNewWeatherInfo(Weather: weather!)
            self.loadUpdatedCities()
            }
            else {
                self.alert(message: "check your internet connection")
            }

        }
        manager.delegate = nil
        manager.stopUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            print("denied")
            
            self.getForecastWeather(cityId : 5056033)
        }
    }
    func getForecastWeather(cityId : Int)  {
        
        
        SVProgressHUD.show()
        ServiceManager.shared.getForecastWeather(by:cityId, completion: {  (weather) in
            SVProgressHUD.dismiss()
            if let _ = weather {
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "WeatherViewController") as! WeatherViewController
                controller.weather = weather!
                controller.refreshCompletion = {
                    self.loadUpdatedCities()
                }
                let popup = PopupDialog(viewController: controller)
                self.present(popup, animated: true, completion: nil)
            }
            else {
                self.alert(message: "check your internet connection")
                
            }
        })
        self.loadUpdatedCities()

    }
    
}


    


extension HomeViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        cell.configurationCell(weatherIcon: nil, city: cities[indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "WeatherViewController") as! WeatherViewController
        let weathers = RealmHelper.shared.loadAllWeather()
        controller.weather = weathers[indexPath.row]
        let popup = PopupDialog(viewController: controller)
        self.present(popup, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle:   UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let allCountryWeather = RealmHelper.shared.loadAllWeather()
            let filterCountry = allCountryWeather.filter { $0.city?.id == self.cities[indexPath.row].id
            }
            cities.remove(at: indexPath.row)
            RealmHelper.shared.clearWeatherItem(obj: filterCountry[0])
            self.tableView.deleteRows(at: [indexPath], with: .automatic)

        }
    }
    
    
    
}
