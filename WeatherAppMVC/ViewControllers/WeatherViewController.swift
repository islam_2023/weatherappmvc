//
//  WeatherViewController.swift
//  WeatherAppMVC
//
//  Created by OSX on 9/11/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import UIKit
import RealmSwift
protocol HideAddButton {
    func hideAddButton()
}

class WeatherViewController: UIViewController ,HideAddButton{
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var addButton: UIButton!
    var refreshCompletion : (() -> ())?
    
    var weather = WeatherDetails()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func addAction(_ sender: Any) {
        let allCountryWeather = RealmHelper.shared.loadAllWeather()
        
        if allCountryWeather.count == 5 {
            self.alert(message: "can't add more than 5 countries")
        }
        else {
            let filterCountry = allCountryWeather.filter { $0.city?.id == self.weather.city?.id}
            let realm = try! Realm()
            var obj = filterCountry.first
                try! realm.write {
                    obj = self.weather
                }
            if filterCountry.count == 0 {
                RealmHelper.shared.addNewWeatherInfo(Weather: self.weather)
                self.dismiss(animated: true, completion: {
                    self.refreshCompletion?()
                })
            }
        }
    }
    func hideAddButton() {
        addButton.isHidden = true
    }
    
    
}

extension WeatherViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weather.weatherInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherTableViewCell", for: indexPath) as! WeatherTableViewCell
        cell.configurationCell(city: weather.city!.name, weather :weather.weatherInfo[indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
    
    
}
