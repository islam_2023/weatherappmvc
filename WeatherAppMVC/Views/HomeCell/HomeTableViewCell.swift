//
//  HomeTableViewCell.swift
//  WeatherAppMVC
//
//  Created by OSX on 9/11/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var cityName: UILabel!
    
    @IBOutlet weak var cityCode: UILabel!
    
    @IBOutlet weak var weatherIcon: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configurationCell(weatherIcon:String? , city : CityModel)  {
        
        if let _ = weatherIcon {
            self.weatherIcon.isHidden = false
            let url = URL(string: weatherIcon!)
            self.weatherIcon.kf.setImage(with: url , placeholder: UIImage(named: "Placeholder") , options: nil, progressBlock: nil, completionHandler: nil)            
        }
        else {
            self.weatherIcon.isHidden = true
        }
        cityName.text = city.name
        cityCode.text = city.country_code
       
    }

}
