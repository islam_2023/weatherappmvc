//
//  ServiceManager.swift
//  WeatherAppMVC
//
//  Created by OSX on 9/11/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import Foundation
import Alamofire

class ServiceManager: DataSourceService {
    
    static let shared = ServiceManager()
    func getForecastWeather(by cityId: Int, completion: @escaping CompletionSingle) {
        
        let url = "\(apiEndpoint)/forecast?id=\(cityId)&appid\(apiKey)"
  
        
        Alamofire.request(url ,
                          method: .get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: nil )
            .responseJSON {(response) in
                switch response.result {
                case .success:
                    do {
                     
                        let weatherResponse = try JSONDecoder().decode(WeatherDetails.self, from: response.data!)
                        
                        completion(weatherResponse)
                    }
                    catch let jsonError {
                        print("Error serializing json:", jsonError)
                        completion(nil)
                        
                    }
                case .failure(let error):
                    print(error)
                    completion(nil)
                }
                
        }
       
        
        
    }
    
    func getForecastWeather(by lat: String, lng: String, completion: @escaping CompletionSingle) {
        let url = "\(apiEndpoint)/forecast?lat=\(lat)&lon=\(lng)&appid\(apiKey)"
        
        Alamofire.request(url ,
                          method: .get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: nil )
            .responseJSON {(response) in
                switch response.result {
                case .success:
                    do {
                   
                        let weatherResponse = try JSONDecoder().decode(WeatherDetails.self, from: response.data!)
                        
                        completion(weatherResponse)
                    }
                    catch let jsonError {
                        print("Error serializing json:", jsonError)
                        completion(nil)
                        
                    }
                case .failure(let error):
                    print(error)
                }
                
        }
    }
    
    
}
