//
//  ServiceHandler.swift
//  WeatherAppMVC
//
//  Created by OSX on 9/11/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import Foundation


let apiKey = "=a3d65b0a2e7c04292d49965360f1de00"
let apiEndpoint = "https://api.openweathermap.org/data/2.5"

protocol DataSourceService {
    typealias CompletionSingle = (_ weather :WeatherDetails?) -> Void
    func getForecastWeather(by cityId: Int, completion: @escaping CompletionSingle)
    func getForecastWeather(by lat: String, lng: String ,completion: @escaping CompletionSingle)
}
