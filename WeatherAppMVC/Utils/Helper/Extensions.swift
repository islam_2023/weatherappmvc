//
//  Extensions.swift
//  WeatherAppMVC
//
//  Created by OSX on 9/12/19.
//  Copyright © 2019 eslam. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func getFormattedDate() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formateDate = dateFormatter.date(from: self)!
        dateFormatter.dateFormat = "MMM d, h:mm a"
        return dateFormatter.string(from: formateDate)
    }
}

extension Double {
    
     func temperatureConvertorKelvinToCelsius() -> String {
        return String( Int (self - 273.15)) + "°C"
    }
}

extension UIViewController {
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
