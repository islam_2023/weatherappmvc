
import Foundation

protocol PersistentService {
    func save(value: Any, forKey key: String)
    func get(forKey key: String) -> Any?
}
