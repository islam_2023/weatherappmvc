

import Foundation

class UserDefaultsPersistentService: PersistentService {
    let defaults = UserDefaults.standard
    
    func save(value: Any, forKey key: String) {
        defaults.set(value, forKey: key)
    }
    
    func get(forKey key: String) -> Any? {
        return defaults.value(forKey: key)
    }
}
