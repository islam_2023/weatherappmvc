//
//  ParseCitiesJsonFile.swift
//  WeatherAppMVC
//
//  Created by OSX on 9/9/19.
//  Copyright © 2019 eslam. All rights reserved.
//
import Foundation
import RealmSwift

class RealmHelper{
    
    let uiRealm = try! Realm()
    
    private init() {}
    
    public static let shared = RealmHelper()
    
    public func loadAllWeather() -> [WeatherDetails]{
        return Array(uiRealm.objects(WeatherDetails.self))
    }
    
    public func addNewWeatherInfo(Weather: WeatherDetails) {
        do {
            try uiRealm.write {
                uiRealm.add(Weather)
                
            }
        } catch let error as NSError {
            print("error - \(error.localizedDescription)")
        }

    }
    
    public func clearWeatherItem(obj: WeatherDetails) {
        do {
            try uiRealm.write {
                uiRealm.delete(obj)
            }
        } catch let error as NSError {
            print("error - \(error.localizedDescription)")
        }
    }
    
    
    public func clearAllWeatherInfo() {
        do {
            try uiRealm.write {
                uiRealm.delete(uiRealm.objects(WeatherDetails.self))
            }
        } catch let error as NSError {
            print("error - \(error.localizedDescription)")
        }
        
    }
}



extension RealmHelper {
    
    
    public func loadAllCities() -> [CityModel]{
        return Array(uiRealm.objects(CityModel.self))
    }
    
    public func addNewCity(obj: CityModel) {
        do {
            try uiRealm.write {
            uiRealm.add(obj)
            }
        } catch let error as NSError {
            print("error - \(error.localizedDescription)")
        }
    }
    
    public func clearCityItem(obj: CityModel) {
        do {
            try uiRealm.write {
                uiRealm.delete(obj)
            }
        } catch let error as NSError {
            print("error - \(error.localizedDescription)")
        }
    }
    
    
    public func clearAllCities() {
        do {
            try uiRealm.write {
                uiRealm.delete(uiRealm.objects(CityModel.self))
            }
        } catch let error as NSError {
            print("error - \(error.localizedDescription)")
        }
        
    }
    
}



extension RealmOptional : Encodable where Value: Encodable  {
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        if let v = self.value {
            try v.encode(to: encoder)
        } else {
            try container.encodeNil()
        }
    }
}
extension RealmOptional : Decodable where Value: Decodable {
    public convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            self.value = try Value(from: decoder)
        }
    }
}
extension List : Decodable where Element : Decodable {
    public convenience init(from decoder: Decoder) throws {
        self.init()
        var container = try decoder.unkeyedContainer()
        while !container.isAtEnd {
            let element = try container.decode(Element.self)
            self.append(element)
        }
    }
}
extension List : Encodable where Element : Encodable {
    public func encode(to encoder: Encoder) throws {
        var container = encoder.unkeyedContainer()
        for element in self {
            try element.encode(to: container.superEncoder())
        }
    }
}
